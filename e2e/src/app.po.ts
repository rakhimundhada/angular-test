import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  getTitleText() {
    return element(by.css('app-root  h2')).getText() as Promise<string>;
  }

  setSearchInputeValue(searchItem){
    return element(by.id('SearchInpute')).sendKeys(searchItem);
  }

  getAllRows(){
    return element.all(by.css('.ui-table-tbody #tableRows'));
  }
}
