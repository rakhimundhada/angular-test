import { AppPage } from './app.po';
import { browser, logging, element ,by} from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    //browser.sleep(10000);
    expect(page.getTitleText()).toEqual('Welcome');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });

  it("Search the item in below table", () => {
    page.navigateTo();
    page.setSearchInputeValue(1987);
    let allRows = page.getAllRows();
    allRows.then(rowsResolved => {
      expect(rowsResolved.length).toBe(6);
    });
  });
});
