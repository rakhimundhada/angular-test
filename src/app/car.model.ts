export interface Car {
    data:Array<carType>
}

export interface carType {
    vin?:string;
    year?:string;
    brand?:string;
    color?:string;
    price?:string;
    saleDate?:string;
}