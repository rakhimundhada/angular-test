import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Car } from './car.model';
import { Observable } from 'rxjs';
import {tap} from 'rxjs/operators'


@Injectable()
export class CarService {

    constructor(private http: HttpClient) { }

    public getCars():Observable<Car> {
        return this.http.get<Car>('./assets/data/carData.json').pipe(tap((data)=>{
            console.log('adata',data);
            return data;
        }))
    }
}