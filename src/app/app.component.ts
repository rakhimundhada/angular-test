import { Component, OnInit } from '@angular/core';
import { Car,carType } from './car.model';
import { CarService } from './carService.service';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'AngularTestProject';
  cols: any[];
  yearTimeout: any;
  brands: any[];
  cars: carType[];
  datasource :carType [];
  loading : boolean = false;
colors: any[];
totalRecords :number;
  constructor(private carService: CarService) { }

  ngOnInit() {
this.carService.getCars().subscribe(cars => {
    this.cars = cars.data;

    //this.datasource = cars.data;
  //  this.totalRecords = this.datasource.length;
    console.log('carData'+cars)});
   this.cols = [
    { field: 'vin', header: 'Vin' },
    { field: 'year', header: 'Year' },
    { field: 'brand', header: 'Brand' },
    { field: 'color', header: 'Color' }
];


this.brands = [
    { label: 'All Brands', value: null },
    { label: 'Audi', value: 'Audi' },
    { label: 'BMW', value: 'BMW' },
    { label: 'Fiat', value: 'Fiat' },
    { label: 'Honda', value: 'Honda' },
    { label: 'Jaguar', value: 'Jaguar' },
    { label: 'Mercedes', value: 'Mercedes' },
    { label: 'Renault', value: 'Renault' },
    { label: 'VW', value: 'VW' },
    { label: 'Volvo', value: 'Volvo' }
];

this.colors = [
    { label: 'White', value: 'White' },
    { label: 'Green', value: 'Green' },
    { label: 'Silver', value: 'Silver' },
    { label: 'Black', value: 'Black' },
    { label: 'Red', value: 'Red' },
    { label: 'Maroon', value: 'Maroon' },
    { label: 'Brown', value: 'Brown' },
    { label: 'Orange', value: 'Orange' },
    { label: 'Blue', value: 'Blue' }
];

this.loading = true;
  }
   
  onYearChange(event, dt) {
    if (this.yearTimeout) {
        clearTimeout(this.yearTimeout);
    }

    this.yearTimeout = setTimeout(() => {
        dt.filter(event.value, 'year', 'gt');
    }, 250);
}

// loadCarsLazy(event: LazyLoadEvent) {
//     this.loading = true;

//     //in a real application, make a remote request to load data using state metadata from event
//     //event.first = First row offset
//     //event.rows = Number of rows per page
//     //event.sortField = Field name to sort with
//     //event.sortOrder = Sort order as number, 1 for asc and -1 for dec
//     //filters: FilterMetadata object having field as key and filter value, filter matchMode as value

//     //imitate db connection over a network
//     setTimeout(() => {
//         if (this.datasource) {
//             this.cars = this.datasource.slice(event.first, (event.first + event.rows));
//             this.loading = false;
//         }
//     }, 1000);
// }
  

}


